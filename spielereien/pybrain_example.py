from pybrain.tools.shortcuts import *
from pybrain.structure import *
net = buildNetwork(2, 4, 1, bias=True, hiddenclass=TanhLayer)

print(net)
print(net["in"])
print(net["out"])
print(net["hidden0"])

from pybrain.datasets import *

ds = SupervisedDataSet(2, 1)
ds.addSample((0, 0), (0,))
ds.addSample((0, 1), (1,))
ds.addSample((1, 0), (1,))
ds.addSample((1, 1), (0,))

from pybrain.supervised.trainers import BackpropTrainer
trainer = BackpropTrainer(net)

if False:
    for i in range(0, 1000):
        err = trainer.train()
        if i % 100 == 0:
            print("err:", err)
else:
    # Train iterative
    for i in range(0, 2000):
        for inpt, target in ds:
            incDs = SupervisedDataSet(2, 1)
            incDs.addSample(inpt, target)
            trainer.setData(incDs)
            err = trainer.train()
            if i % 100 == 0:
                print("err:", err)

#print(net.activate([1, 2]))
#trainer.trainUntilConvergence()


for inp, target in ds:
    print(inp, target, net.activate(inp))
