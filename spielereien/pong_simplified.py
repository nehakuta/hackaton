import pygame, sys
from pygame.locals import *

# Number of frames per second
# Change this value to speed up or slow down your game
FPS = 200

#Global Variables to be used through our program

WINDOWWIDTH = 400
WINDOWHEIGHT = 300
LINETHICKNESS = 10
PADDLESIZE = 50
PADDLEOFFSET = 20

UI = True

# Set up the =   colours
BLACK     = (0  ,0  ,0  )
WHITE     = (255,255,255)

class Pong:
    def __init__(self):
        self.DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH,WINDOWHEIGHT))
        pygame.display.set_caption('Pong')

        #Initiate variable and set starting positions
        #any future changes made within rectangles
        ballX = WINDOWWIDTH/2 - LINETHICKNESS/2
        ballY = WINDOWHEIGHT/2 - LINETHICKNESS/2
        playerOnePosition = (WINDOWHEIGHT - PADDLESIZE) /2
        playerTwoPosition = (WINDOWHEIGHT - PADDLESIZE) /2

        #Keeps track of ball direction
        self.ballDirX = -1 ## -1 = left 1 = right
        self.ballDirY = -1 ## -1 = up 1 = down

        #Creates Rectangles for ball and paddles.
        self.paddle1 = pygame.Rect(PADDLEOFFSET,playerOnePosition, LINETHICKNESS,PADDLESIZE)
        self.paddle2 = pygame.Rect(WINDOWWIDTH - PADDLEOFFSET - LINETHICKNESS, playerTwoPosition, LINETHICKNESS,PADDLESIZE)
        self.ball = pygame.Rect(ballX, ballY, LINETHICKNESS, LINETHICKNESS)

    #Draws the arena the game will be played in.
    def drawArena(self):
        self.DISPLAYSURF.fill((0,0,0))
        #Draw outline of arena
        pygame.draw.rect(self.DISPLAYSURF, WHITE, ((0,0),(WINDOWWIDTH,WINDOWHEIGHT)), LINETHICKNESS*2)
        #Draw centre line
        pygame.draw.line(self.DISPLAYSURF, WHITE, ((WINDOWWIDTH//2),0),((WINDOWWIDTH//2),WINDOWHEIGHT), (LINETHICKNESS//4))

    #Draws the paddle
    def drawPaddle(self, paddle):
        #Stops paddle moving too low
        if paddle.bottom > WINDOWHEIGHT - LINETHICKNESS:
            paddle.bottom = WINDOWHEIGHT - LINETHICKNESS
        #Stops paddle moving too high
        elif paddle.top < LINETHICKNESS:
            paddle.top = LINETHICKNESS
        #Draws paddle
        pygame.draw.rect(self.DISPLAYSURF, WHITE, paddle)

    #draws the ball
    def drawBall(self):
        pygame.draw.rect(self.DISPLAYSURF, WHITE, self.ball)

    #moves the ball returns new position
    def moveBall(self):
        self.ball.x += self.ballDirX
        self.ball.y += self.ballDirY

    #Checks for a collision with a wall, and 'bounces' ball off it.
    #Returns new direction
    def checkEdgeCollision(self):
        if self.ball.top <= (LINETHICKNESS) or self.ball.bottom >= (WINDOWHEIGHT - LINETHICKNESS):
            self.ballDirY = self.ballDirY * -1
        if self.ball.left <= (LINETHICKNESS) or self.ball.right >= (WINDOWWIDTH - LINETHICKNESS):
            self.ballDirX = self.ballDirX * -1

    #Checks is the ball has hit a paddle, and 'bounces' ball off it.
    def checkHitBall(self):
        if self.ballDirX < 0 and self.paddle1.right >= self.ball.left and self.paddle1.top <= self.ball.bottom and self.paddle1.bottom >= self.ball.top:
            return -1
        elif self.ballDirX > 0 and self.paddle2.left <= self.ball.right and self.paddle2.top <= self.ball.bottom and self.paddle2.bottom >= self.ball.top:
            return -1
        else: return 1

    #Checks to see if a point has been scored returns new score
    def getReward(self):
        #reset points if left wall is hit
        if self.ball.left <= LINETHICKNESS:
            return -1.0
        #1 point for hitting the ball
        elif self.ballDirX < 0 and self.paddle1.right >= self.ball.left and self.paddle1.top <= self.ball.bottom and self.paddle1.bottom >= self.ball.top:
            return 1.0
        #5 points for beating the other paddle
        elif self.ball.right > WINDOWWIDTH - LINETHICKNESS:
            return 5.0
        #if no points scored, return score unchanged
        else: return 0.0
        
    def getError(self):
        if self.ball.left <= self.paddle1.right:
            if self.ball.bottom < self.paddle1.top:
                return self.paddle1.top - self.ball.bottom
            elif self.ball.top > self.paddle1.bottom:
                return self.ball.top - self.paddle1.bottom
            return 0
        return 1

    #Artificial Intelligence of computer player
    def artificialIntelligence(self):
        #If ball is moving away from paddle, center bat
        if self.ballDirX == -1:
            if self.paddle2.centery < (WINDOWHEIGHT/2):
                self.paddle2.y += 1
            elif self.paddle2.centery > (WINDOWHEIGHT/2):
                self.paddle2.y -= 1
        #if ball moving towards bat, track its movement.
        elif self.ballDirX == 1:
            if self.paddle2.centery < self.ball.centery:
                self.paddle2.y += 1
            else:
                self.paddle2.y -=1
        return self.paddle2

    # Run the game logic for one iteration and return the reward
    def doGame(self, paddle_y):
        self.paddle1.y = paddle_y

        if UI:
            self.drawArena()
            self.drawPaddle(self.paddle1)
            self.drawPaddle(self.paddle2)
            self.drawBall()
        self.moveBall()
        self.checkEdgeCollision()
        reward = self.getReward()
        self.ballDirX = self.ballDirX * self.checkHitBall()
        self.paddle2 = self.artificialIntelligence()

        # Note: Normalize all values to 1.0. We don
        return (reward, \
                self.paddle1.x / WINDOWWIDTH, self.paddle1.y / WINDOWHEIGHT, \
                self.paddle2.x / WINDOWWIDTH, self.paddle2.y / WINDOWHEIGHT, \
                self.ball.x / WINDOWWIDTH, self.ball.y / WINDOWHEIGHT)

#Main function
def main():
    pygame.init()
    pygame.font.init()
    myfont = pygame.font.SysFont("Sans", 10)

    FPSCLOCK = pygame.time.Clock()

    game = Pong()

    rewardVal = 0 # initial value

    while True: #main game loop
        paddle1_y = game.paddle1.y
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            # mouse movement commands
            elif event.type == MOUSEMOTION:
                mousex, mousey = event.pos
                paddle1_y = mousey

        # TODO: connect ANN here

        res = game.doGame(paddle1_y)

        if UI: # set to false to speed things up
            pygame.display.update()
            print(res)
            FPSCLOCK.tick(FPS)

if __name__=='__main__':
    main()
